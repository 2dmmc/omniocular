package me.exz.omniocular.util;

import com.google.gson.*;
import net.minecraft.nbt.*;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.Set;

import static me.exz.omniocular.util.NBTHelper.NBTCache;
import static me.exz.omniocular.util.NBTHelper.gson;

class NBTSerializer implements JsonSerializer<NBTBase> {
    @Override
    public JsonElement serialize(NBTBase src, Type typeOfSrc, JsonSerializationContext context) {
        switch (src.getId()) {
            case 0:
                return JsonNull.INSTANCE;
            case 1:
                return new JsonPrimitive(((NBTTagByte) src).getByte());
            case 2:
                return new JsonPrimitive(((NBTTagShort) src).getShort());
            case 3:
                return new JsonPrimitive(((NBTTagInt) src).getInt());
            case 4:
                return new JsonPrimitive(((NBTTagLong) src).getLong());
            case 5:
                return new JsonPrimitive(((NBTTagFloat) src).getFloat());
            case 6:
                return new JsonPrimitive(((NBTTagDouble) src).getDouble());
            case 7:
                JsonArray jsonArrayByte = new JsonArray();
                for (byte b : ((NBTTagByteArray) src).getByteArray()) {
                    jsonArrayByte.add(new JsonPrimitive(b));
                }
                return jsonArrayByte;
            case 8:
                return new JsonPrimitive(((NBTTagString) src).getString());
            case 9:
                JsonArray jsonArrayList = new JsonArray();
                NBTTagList list = ((NBTTagList) src);
                for (int i = 0; i < list.tagCount(); i++) {
                    jsonArrayList.add(gson.toJsonTree(list.get(i)));
                }
                return jsonArrayList;
            case 10:
                JsonObject jsonObject = new JsonObject();
                NBTTagCompound nbtTagCompound = (NBTTagCompound) src;
                int hashCode = nbtTagCompound.hashCode();
                NBTCache.put(hashCode, nbtTagCompound);
                jsonObject.add("hashCode", new JsonPrimitive(hashCode));
                Set<String> keySet = nbtTagCompound.getKeySet();
                for (String key : keySet) {
                    jsonObject.add(key, gson.toJsonTree(nbtTagCompound.getTag(key)));
                }
                return jsonObject;
            case 11:
                JsonArray jsonArrayInt = new JsonArray();
                for (int i : ((NBTTagIntArray) src).getIntArray()) {
                    jsonArrayInt.add(new JsonPrimitive(i));
                }
                return jsonArrayInt;
            default:
                return null;
        }
    }
}
