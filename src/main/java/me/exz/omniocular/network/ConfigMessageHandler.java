package me.exz.omniocular.network;

import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import me.exz.omniocular.reference.Reference;

public class ConfigMessageHandler implements IMessageHandler<ConfigMessage, IMessage> {
    public static final SimpleNetworkWrapper network = NetworkRegistry.INSTANCE.newSimpleChannel(Reference.MOD_ID);

    @Override
    public IMessage onMessage(ConfigMessage message, MessageContext ctx) {
        //LogHelper.info("Config Received: "+ message.text);
        NetworkHelper.recvConfigString(message.text);
        return null;
    }
}