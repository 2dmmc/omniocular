package me.exz.omniocular.asm;

import com.google.common.eventbus.EventBus;
import net.minecraftforge.fml.common.DummyModContainer;
import net.minecraftforge.fml.common.LoadController;
import net.minecraftforge.fml.common.ModMetadata;

import java.util.Arrays;

public class CoreContainer extends DummyModContainer {
    public CoreContainer() {
        super(new ModMetadata());
        ModMetadata meta = getMetadata();
        meta.modId = "OmniOcularCore";
        meta.name = "Omni Ocular Core";
        meta.version = "1.0";
        meta.authorList = Arrays.asList("Epix");
        meta.description = "A CoreMod to inject into Waila method";
        meta.url = "http://exz.me";
    }

    @Override
    public boolean registerBus(EventBus bus, LoadController controller) {
        bus.register(this);
        return true;
    }
}
