package me.exz.omniocular.command;

import me.exz.omniocular.handler.ConfigHandler;
import me.exz.omniocular.network.NetworkHelper;
import me.exz.omniocular.util.LogHelper;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.management.PlayerList;

import java.util.List;

public class CommandReloadConfig extends CommandBase {
    @Override
    public String getCommandName() {
        return "oor";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 3;
    }

    @Override
    public boolean checkPermission(MinecraftServer minecraftServer, ICommandSender sender) {
        return minecraftServer.isSinglePlayer() || super.checkPermission(minecraftServer, sender);
    }

    @Override
    public String getCommandUsage(ICommandSender iCommandSender) {
        return "/oor";
    }

    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender iCommandSender, String[] strings) throws CommandException {
        ConfigHandler.mergeConfig();
        List playerList = minecraftServer.getPlayerList().getPlayerList();
        for (Object player : playerList) {
            //ConfigMessageHandler.network.sendTo(new ConfigMessage(ConfigHandler.mergedConfig), (EntityPlayerMP) player);
            NetworkHelper.sendConfigString(ConfigHandler.mergedConfig, (EntityPlayerMP) player);
        }
        LogHelper.info(iCommandSender.getDisplayName() + " commit a config reload.");
    }

}