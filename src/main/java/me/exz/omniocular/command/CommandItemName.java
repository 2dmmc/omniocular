package me.exz.omniocular.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentTranslation;

public class CommandItemName extends CommandBase {
    @Override
    public String getCommandName() {
        return "ooi";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "/ooi";
    }

    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender iCommandSender, String[] strings) throws CommandException {
        EntityPlayer player = (EntityPlayer) iCommandSender;
        ItemStack holdItem = player.getHeldItem(EnumHand.MAIN_HAND);
        if (holdItem == null) {
            player.addChatComponentMessage(new TextComponentTranslation("omniocular.info.NotHolding"));
            return;
        }
        player.addChatComponentMessage(new TextComponentTranslation(holdItem.getItem().getUnlocalizedName()));
    }
}