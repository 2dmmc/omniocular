package me.exz.omniocular.command;

import net.minecraft.client.Minecraft;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;

public class CommandEntityName extends CommandBase {
    @Override
    public String getCommandName() {
        return "ooe";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "/ooe";
    }

    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender iCommandSender, String[] strings) throws CommandException {
        EntityPlayer player = (EntityPlayer) iCommandSender;
        Minecraft minecraft = Minecraft.getMinecraft();
        RayTraceResult objectMouseOver = minecraft.objectMouseOver;
        if (objectMouseOver.typeOfHit == RayTraceResult.Type.ENTITY) {
            Class pointEntityClass = objectMouseOver.entityHit.getClass();
            if (EntityList.CLASS_TO_NAME.containsKey(pointEntityClass)) {
                player.addChatComponentMessage(new TextComponentString(EntityList.getEntityString(objectMouseOver.entityHit)));
            }
        } else {
            player.addChatComponentMessage(new TextComponentTranslation("omniocular.info.NotPointing"));
        }
    }
}