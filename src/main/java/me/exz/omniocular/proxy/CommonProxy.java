package me.exz.omniocular.proxy;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.relauncher.Side;
import me.exz.omniocular.command.CommandReloadConfig;
import me.exz.omniocular.event.ConfigEvent;
import me.exz.omniocular.handler.ConfigHandler;
import me.exz.omniocular.network.ConfigMessage;
import me.exz.omniocular.network.ConfigMessageHandler;

public abstract class CommonProxy implements IProxy {
    @Override
    public void registerServerCommand(FMLServerStartingEvent event) {
        event.registerServerCommand(new CommandReloadConfig());
    }

    @Override
    public void registerEvent() {
        MinecraftForge.EVENT_BUS.register(new ConfigEvent());
    }

    @Override
    public void registerNetwork() {
        ConfigMessageHandler.network.registerMessage(ConfigMessageHandler.class, ConfigMessage.class, 0, Side.CLIENT);

    }

    @Override
    public void initConfig(FMLPreInitializationEvent event) {
        ConfigHandler.minecraftConfigDirectory = event.getModConfigurationDirectory();
        ConfigHandler.initConfigFiles();
        //JSHandler.initEngine();
    }
}
