package me.exz.omniocular.proxy;

import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

public interface IProxy {
    void registerEvent();

    void registerClientCommand();

    void registerServerCommand(FMLServerStartingEvent event);

    void registerWaila();

    void registerNEI();

    void registerNetwork();

    void initConfig(FMLPreInitializationEvent event);

    void prepareConfigFiles();

    void startHttpServer();
}
